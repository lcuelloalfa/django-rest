# Test de Django REST API

## Acerca de este repositorio

Basicamente se probaron las funcionalidades de Django como REST API, el proyecto consiste en un administrador de tareas, las rutas a excepción de login y register estan protegidas. Para probar las rutas protegidas con postman se debe copiar el token de login (access_token) en Authorization>Bearer Token

## Instalación

### Clonar este repositorio

```
git clone https://lcuelloalfa@bitbucket.org/lcuelloalfa/django-rest.git
```

### Entrar a la carpeta django-rest

```
cd django-rest
```

### Crear el contenedor

```
docker build -t rest .
```

### Iniciar el contenedor

```
docker run -d -p 8001:8000 rest
```

## API Endpoints

### Register

En Postman, crear un metodo POST con la ruta:

```
http://localhost:8001/login/register
```

En el body>form-data agregar los siguientes parametros:
```
username (STRING), password (STRING, 8 caracteres), email (STRING, email), firstname (STRING, opcional), lastname (STRING, opcional)
```

### Login

En Postman, iniciar sesión con la opción de Authorization>Basic Auth, introducir el username y password utilizados en register

```
http://localhost:8001/login
```

### ListTasks

Crear lista de tareas (POST):

```
http://localhost:8001/lists/add
```

Parámetros: name (CHAR[50]), description (TEXT)

Leer listas de tareas (GET):

```
http://localhost:8001/lists/list
```


### Tasks

Crear tareas:

```
http://localhost:8001/tasks/add
```

Parametros: 
```
name(STRING), description(STRING), done(BOOLEAN,opcional), list_id(INT)
```

Leer tareas

```
http://localhost:8001/tasks/list?list_id=X
```

Parametros: list_id (INT)

