asgiref==3.2.10
Django==3.0.8
django-utils-six==2.0
djangorestframework==3.11.0
PyJWT==1.7.1
pytz==2020.1
sqlparse==0.3.1
uWSGI==2.0.19.1
